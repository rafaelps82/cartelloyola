import './css/site.css';
import './css/custom.css';
import './css/font-awesome.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store'
import * as a from './global'
Vue.use(VueRouter);


const routes = [
    { path: '/login', component: require('./components/login/login.vue.html') },
    { path: '/dashboard', component: require('./components/home/home.vue.html'), beforeEnter: requireAuth },

    { path: '/usuario', component: require('./components/usuario/usuario.vue.html'), beforeEnter: requireAuth },
    { path: '/usuario/cadastrar', component: require('./components/usuario/cadastrar.vue.html'), beforeEnter: requireAuth },
    { path: '/usuario/editar/:id', component: require('./components/usuario/editar.vue.html'), beforeEnter: requireAuth },

    { path: '/produto', component: require('./components/produto/produto.vue.html'), beforeEnter: requireAuth },
    { path: '/produto/cadastrar', component: require('./components/produto/cadastrar.vue.html'), beforeEnter: requireAuth },
    { path: '/produto/editar/:id', component: require('./components/produto/editar.vue.html'), beforeEnter: requireAuth },

    { path: '/', redirect: '/dashboard' }
];

function requireAuth(to: any, from: any, next: any) {

    const auth: any = store.getters.isLoggedIn;

    if (!auth) {
        next('/login')
    } else {
        next()
    }
}

new Vue({
    el: '#app-root',
    store,
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});

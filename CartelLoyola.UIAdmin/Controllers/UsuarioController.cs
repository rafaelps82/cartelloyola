﻿using System;
using CartelLoyola.Domain;
using CartelLoyola.Service;
using CartelLoyola.Data;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using System.Net.Http;

namespace CartelLoyola.UIAdmin.Controllers
{
    [Route("api/[controller]")]
    public class UsuarioController : Controller
    {
        CartelLoyolaContext _dbContext = new CartelLoyolaContext();
        UsuarioAdminService _usuarioService = new UsuarioAdminService();

        [HttpPost("[action]")]
        public IActionResult SalvarUsuario([FromBody] UsuarioAdmin b)
        {

            var user = _dbContext.UsuarioAdmin.Where(a => a.Username.Equals(b.Username)).FirstOrDefault();

            if (user != null) {
                if (b.Id == 0 && user.Username == b.Username)
                {
                    throw new Exception("Usuario já existe");
                }
            }

            using (MD5 md5Hash = MD5.Create())
            {
                UsuarioAdmin usuario = new UsuarioAdmin
                {
                    Id = b.Id,
                    Nome = b.Nome,
                    Email = b.Email,
                    Username = b.Username,
                    Password = Utils.GetMd5Hash(md5Hash, b.Password),
                };

                if (usuario.Id == 0)
                {
                    _usuarioService.Salvar(usuario);
                }
                else
                {
                    _usuarioService.Atualizar(usuario);
                }

                return Ok(usuario);
            }
        }

        [HttpGet("[action]")]
        public IActionResult ObterTodos()
        {

            var usuarios = _usuarioService.ObterTodos();

            return Ok(usuarios);
        }

        [HttpPost("[action]")]
        public JsonResult ObterPor([FromBody] int id)
        {

            var item = _usuarioService.ObterPor(id);

            return Json(item);
        }

        [HttpPost("[action]")]
        public string Deletar([FromBody] int id)
        {
            _usuarioService.Deletar(id);

            return "Usuario deletado";
        }


    }
}
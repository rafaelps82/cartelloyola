﻿using System;
using CartelLoyola.Domain;
using CartelLoyola.Service;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Drawing;

namespace CartelLoyola.UIAdmin.Controllers
{
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        ProdutoService _produtoService = new ProdutoService();

        [HttpPost("[action]")]
        public IActionResult SalvarProduto([FromBody] Produto b)
        {

            var produto = new Produto
            {
                Id = b.Id,
                Nome = b.Nome,
                Descricao = b.Descricao,
                Valor = b.Valor,
                ImagemProduto = b.ImagemProduto
            };

            if (produto.Id == 0)
            {
                _produtoService.Salvar(produto);
            }
            else
            {
                _produtoService.Atualizar(produto);
            }

            return Ok(produto);
        }

        [HttpGet("[action]")]
        public IActionResult ObterTodos()
        {

           var produtos = _produtoService.ObterTodos();

            return Ok(produtos);
        }

        [HttpPost("[action]")]
        public JsonResult ObterPor([FromBody] int id)
        {
            var item = _produtoService.ObterPor(id);

            return Json(item);
        }

        [HttpPost("[action]")]
        public string Deletar([FromBody] int id)
        {
            _produtoService.Deletar(id);

            return "Item deletado";
        }

    }
}
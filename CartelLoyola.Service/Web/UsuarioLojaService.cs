﻿using CartelLoyola.Domain;
using CartelLoyola.Repository;
using CartelLoyola.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CartelLoyola.Service
{
    public class UsuarioLojaService
    {
        UsuarioLojaRepository _usuarioRepository;
        CartelLoyolaContext _dbContext = new CartelLoyolaContext();

        public UsuarioLojaService() {
            _usuarioRepository = new UsuarioLojaRepository();
        }

        public void Salvar(UsuarioLoja usuario)
        {
            _usuarioRepository.Salvar(usuario);
        }

        public object ObterTodos()
        {
            return new UsuarioLojaRepository().ObterTodos();
        }

        public object ObterPor(int id)
        {
            return new UsuarioLojaRepository().ObterPor(id);
        }

        public void Atualizar(UsuarioLoja usuario)
        {
            _usuarioRepository.Atualizar(usuario);
        }

        public void Deletar(int id)
        {
            _usuarioRepository.Deletar(id);
        }

    }
}

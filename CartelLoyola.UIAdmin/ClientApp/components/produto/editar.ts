﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface FileReaderEventTarget extends EventTarget {
    result: string
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

@Component
export default class EditarProdutoComponent extends Vue {

    id: number;
    nome: string = '';
    descricao: string = '';
    valor: string = '';
    files: string = '';
    imagem: string = '';

    mounted() {

        fetch('api/Produto/ObterPor', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.$route.params.id)
            })
            .then(response => response.json())
            .then(data => {
                this.id = data.id;
                this.nome = data.nome;
                this.descricao = data.descricao;
                this.valor = data.valor;
                this.imagem = data.imagemProduto;
            });
    }

    Upload(event: any): void {

        this.files = event.target.files[0];
        var input = event.target;

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = (e: FileReaderEvent) => {
                this.imagem = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
        }

    }


    EditarProduto(): void {

        let dados = {
            Id: this.id,
            Nome: this.nome,
            Descricao: this.descricao,
            Valor: this.valor,
            ImagemProduto: this.imagem,
        }

        fetch('api/Produto/SalvarProduto', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dados)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = "/produto"
            });

    }

    Deletar(): void {
        fetch('api/Produto/Deletar', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.id)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = "/produto"
            });
    }
}
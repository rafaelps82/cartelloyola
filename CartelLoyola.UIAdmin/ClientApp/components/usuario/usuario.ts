﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface Usuarios {
    id: number;
    nome: string;
    username: string;
    password: string;
}

@Component
export default class UsuarioComponent extends Vue {

    usuarios: Usuarios[] = [];

    mounted() {
        fetch('api/Usuario/ObterTodos')
            .then(response => response.json() as Promise<Usuarios[]>)
            .then(data => {
                this.usuarios = data;
            });
    }

    Deletar(id: any): void {
        fetch('api/Usuario/Deletar', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(id)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = "/usuario"
            });
    }
}
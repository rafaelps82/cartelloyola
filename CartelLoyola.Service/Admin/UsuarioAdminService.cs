﻿using CartelLoyola.Domain;
using CartelLoyola.Repository;
using CartelLoyola.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CartelLoyola.Service
{
    public class UsuarioAdminService
    {
        UsuarioAdminRepository _usuarioRepository;
        CartelLoyolaContext _dbContext = new CartelLoyolaContext();

        public UsuarioAdminService() {
            _usuarioRepository = new UsuarioAdminRepository();
        }

        public void Salvar(UsuarioAdmin usuario)
        {
            _usuarioRepository.Salvar(usuario);
        }

        public object ObterTodos()
        {
            return new UsuarioAdminRepository().ObterTodos();
        }

        public object ObterPor(int id)
        {
            return new UsuarioAdminRepository().ObterPor(id);
        }

        public void Atualizar(UsuarioAdmin usuario)
        {
            _usuarioRepository.Atualizar(usuario);
        }

        public void Deletar(int id)
        {
            _usuarioRepository.Deletar(id);
        }

    }
}

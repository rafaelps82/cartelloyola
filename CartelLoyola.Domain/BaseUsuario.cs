﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CartelLoyola.Domain
{
    public class BaseUsuario : BaseModelo
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}

import Vue from 'vue';
import { Component } from 'vue-property-decorator';
//import * as a from '../../global';
import Vuex from 'vuex';
import store from '../../store/'

@Component({
    components: {
        MenuComponent: require('../navmenu/navmenu.vue.html')
    }
})
export default class AppComponent extends Vue {
    //username: string = a.username;
    isLoggedIn: boolean = this.$store.getters.isLoggedIn;
    useName: string = this.$store.getters.userName;

    Logout(): void {
        this.$store.dispatch('logout');
    }
}

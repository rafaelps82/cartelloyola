﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class EditarUsuarioComponent extends Vue {

    id: number;
    nome: string = '';
    email: string = '';
    username: string = '';
    password: string = '';

    mounted() {

        fetch('api/Usuario/ObterPor', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.$route.params.id)
        })
            .then(response => response.json())
            .then(data => {
                this.id = data.id;
                this.nome = data.nome;
                this.email = data.email;
                this.username = data.username;
                this.password = data.password;
            });
    }

    EditarUsuario(): void {

        let dados = {
            Id: this.id,
            Nome: this.nome,
            Email: this.email,
            Username: this.username,
            Password: this.password,
        }

        fetch('api/Usuario/SalvarUsuario', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dados)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = "/usuario"
            });

    }

    //Deletar(): void {
    //    fetch('api/Usuario/Deletar', {
    //        method: 'post',
    //        headers: {
    //            'Accept': 'application/json',
    //            'Content-Type': 'application/json',
    //        },
    //        body: JSON.stringify(this.id)
    //    })
    //        .then(response => response.json())
    //        .then(data => {
    //            window.location.href = "/usuario"
    //        });
    //}
}
import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store'
Vue.use(VueRouter);

const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },

    { path: '/produtos', component: require('./components/produto/produtos.vue.html') },
    { path: '/produto/detalhe/:id', component: require('./components/produto/detalhe.vue.html') },
    { path: '/counter', component: require('./components/counter/counter.vue.html') },
    { path: '/cadastrar', component: require('./components/cadastrar/cadastrar.vue.html') },
    //{ path: '/fetchdata', component: require('./components/fetchdata/fetchdata.vue.html') }
];

new Vue({
    el: '#app-root',
    store,
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});

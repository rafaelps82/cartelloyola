﻿using System;
using CartelLoyola.Repository;
using System.Collections.Generic;
using System.Text;
using CartelLoyola.Domain;
using Microsoft.EntityFrameworkCore;

namespace CartelLoyola.Data
{
    public partial class CartelLoyolaContext : DbContext
    {

        public CartelLoyolaContext() 
        {

        }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<UsuarioLoja> UsuarioLoja { get; set; }
        public DbSet<UsuarioAdmin> UsuarioAdmin { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"Server=DESKTOP-AOFM6LE\SQLEXPRESS;Database=CartelLoyolaDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Produto>().ToTable("Produto");
            modelBuilder.Entity<UsuarioLoja>().ToTable("usuario_loja");
            modelBuilder.Entity<UsuarioAdmin>().ToTable("usuario_admin");
        }
    }
}

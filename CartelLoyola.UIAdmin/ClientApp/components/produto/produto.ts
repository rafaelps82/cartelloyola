﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface Produtos {
    id: number;
    nome: string;
    descricao: string;
    valor: string;
    imagem: string;
}

@Component
export default class ProdutoComponent extends Vue {

    produtos: Produtos[] = [];

    mounted() {
        fetch('api/Produto/ObterTodos')
            .then(response => response.json() as Promise<Produtos[]>)
            .then(data => {
                this.produtos = data;
            });
    }

    Deletar(id:any): void {
        fetch('api/Produto/Deletar', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(id)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href = "/produto"
            });
    }
}
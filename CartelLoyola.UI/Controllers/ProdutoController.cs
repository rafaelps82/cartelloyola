﻿using System;
using CartelLoyola.Domain;
using CartelLoyola.Service;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace CartelLoyola.UI.Controllers
{
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        ProdutoService _produtoService = new ProdutoService();

        [HttpGet("[action]")]
        public IActionResult ObterTodos()
        {

            var produtos = _produtoService.ObterTodos();

            return Ok(produtos);
        }

        [HttpPost("[action]")]
        public JsonResult ObterPor([FromBody] int id)
        {
            var item = _produtoService.ObterPor(id);

            return Json(item);
        }
    }
}
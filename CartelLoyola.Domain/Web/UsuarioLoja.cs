﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CartelLoyola.Domain
{
    public class UsuarioLoja: BaseUsuario
    {
        public string Sobrenome { get; set; }
        public Produto Produto { get; set; }
    }
}

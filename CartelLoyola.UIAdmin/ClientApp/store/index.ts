﻿import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";

export default new Vuex.Store({
    state: {
        isLoggedIn: !!localStorage.getItem("TOKEN"),
        userName: localStorage.getItem("USERNAME"),
    },
    mutations: {
        [LOGIN](state) {
            //state.pending = true;
        },
        [LOGIN_SUCCESS](state) {
            state.isLoggedIn = true;
            //state.pending = false;
        },
        [LOGOUT](state) {
            state.isLoggedIn = false;
        }
    },
    actions: {
        login({ commit }, dados) {
            commit(LOGIN); // show spinner
            fetch('api/Login/LoginUsuario', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(dados)
            })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    commit(LOGIN_SUCCESS);
                    localStorage.setItem('TOKEN', data.token);
                    localStorage.setItem('USERNAME', data.username);
                    window.location.href = "/dashboard"
                } else {
                    localStorage.removeItem('TOKEN');
                }
            });
        },
        logout({ commit }) {
            localStorage.removeItem("TOKEN");
            localStorage.removeItem("USERNAME");
            commit(LOGOUT);
        }
    },
    getters: {
        isLoggedIn: state => {
            return state.isLoggedIn
        },
        userName: state => {
            return state.userName
        }
    }
})
﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface Produtos {
    id: number;
    nome: string;
    descricao: string;
    valor: string;
    imagem: string;
}

@Component
export default class ProdutosComponent extends Vue {

    produtos: Produtos[] = [];

    mounted() {
        fetch('api/Produto/ObterTodos')
            .then(response => response.json() as Promise<Produtos[]>)
            .then(data => {
                console.log(data);
                this.produtos = data;
            });
    }
}
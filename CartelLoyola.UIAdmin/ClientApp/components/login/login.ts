﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class LoginComponent extends Vue {

    user: string = '';
    pass: string = '';

    Login(): void {

        let dados = {
            Username: this.user,
            Password: this.pass
        }

        this.$store.dispatch('login', dados);

    }
}
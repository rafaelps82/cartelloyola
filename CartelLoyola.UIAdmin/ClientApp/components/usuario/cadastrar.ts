﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import * as toastr from "toastr";


@Component
export default class CadastrarUsuarioComponent extends Vue {

    nome: string = '';
    email: string = '';
    username: string = '';
    password: string = '';

    SalvarUsuario(): void {


        let dados = {
            Nome: this.nome,
            Email: this.email,
            Username: this.username,
            Password: this.password,
        }

        fetch('api/Usuario/SalvarUsuario', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dados)
        })
            .then(response => response.json())
            .then(data => {
                //toastr.error(data)
                window.location.href = "/usuario/cadastrar"
            })
            

    }
}
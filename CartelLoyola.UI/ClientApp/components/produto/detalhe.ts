﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class DetalheProdutoComponent extends Vue {

    id: number;
    nome: string = '';
    descricao: string = '';
    files: string = '';
    imagem: string = '';

    mounted() {

        fetch('api/Produto/ObterPor', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.$route.params.id)
        })
            .then(response => response.json())
            .then(data => {
                console.log(data)
                this.id = data.id;
                this.nome = data.nome;
                this.descricao = data.descricao;
                this.imagem = data.imagemProduto;
            });
    }
}
﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

interface FileReaderEventTarget extends EventTarget {
    result: string
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

@Component
export default class CadastrarProdutoComponent extends Vue {

    nome: string = '';
    descricao: string = '';
    valor: string = '';
    files: string = '';
    imagem: string = '';


    Upload(event: any): void {

        this.files = event.target.files[0];
        var input = event.target;

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = (e: FileReaderEvent) => {
                this.imagem = e.target.result;
            }

            reader.readAsDataURL(input.files[0]);
        }

    }

    SalvarProduto(): void {


        let dados = {
            Nome: this.nome,
            Descricao: this.descricao,
            Valor: this.valor,
            ImagemProduto: this.imagem,
        }

        fetch('api/Produto/SalvarProduto', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dados)
        })
            .then(response => response.json())
            .then(data => {
                window.location.href="/produto/cadastrar"
            });

    }
}